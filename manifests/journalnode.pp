class hadoop::journalnode (
    String $package_ensure,
) {
    include ::hadoop

    package {'hadoop-hdfs-journalnode':
        ensure => $package_ensure,
    }

    file {'/hadoop/journal':
        ensure => 'directory',
        owner => 'hdfs',
        group => 'hdfs',
        mode => '0755',
        require => Package['hadoop'],
    }

    file {'/usr/lib/systemd/system/hadoop-hdfs-journalnode.service':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/usr/lib/systemd/system/hadoop-hdfs-journalnode.service',
        require => Package['hadoop-hdfs-journalnode'],
        notify  => [Service['hadoop-hdfs-journalnode'], Exec['hadoop daemon-reload']],
    }

    file {'/etc/sysconfig/hadoop-hdfs-journalnode':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/etc/sysconfig/hadoop-hdfs-journalnode',
        require => Package['hadoop-hdfs-journalnode'],
        notify  => Service['hadoop-hdfs-journalnode'],
    }


    service {'hadoop-hdfs-journalnode': }
}
