class hadoop::zookeeper (
    Boolean $enable = true,
    Optional[Integer] $force_id = 0,
    Integer $maxClientCnxns,
    Integer $tickTime,
    Integer $initLimit,
    Integer $syncLimit,
    String $dataDir,
    Integer $defaultClientPort,
    Integer $defaultPeerPort,
    Integer $defaultElectionPort,
    Hash[String, Struct[{id => Integer,
                         Optional[clientPort] => Integer,
                         Optional[peerPort] => Integer,
                         Optional[electionPort] => Integer}]] $zookeepers = {},
) {
    if $enable {
        $package_ensure = 'present'
        $file_ensure = 'file'
        $service_ensure = 'running'
    } else {
        $package_ensure = 'absent'
        $file_ensure = 'absent'
        $service_ensure = 'stopped'
    }

    package {'zookeeper': ensure => $package_ensure, }
    package {'zookeeper-server': ensure => $package_ensure, }

    exec {"zookeeper daemon-reload":
        command     => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    $myself = $zookeepers[$facts['hostname']]
    if $myself {
        $my_id = $myself['id']
    } elsif $force_id > 0 {
        $my_id = $force_id
    }

    if $myself or $force_id > 0 {
        file {"${dataDir}/myid":
            ensure => "${file_ensure}",
            owner => 'zookeeper',
            group => 'zookeeper',
            mode => '0644',
            content => "${my_id}\n",
            notify => Service['zookeeper-server'],
            require => Package['zookeeper'],
        }
    } else {
        file {"${dataDir}/myid":
            ensure => absent,
            notify => Service['zookeeper-server'],
        }
    }

    file { '/etc/zookeeper/conf/zoo.cfg':
        ensure => $file_ensure,
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('hadoop/zoo.cfg.epp', {
            'maxClientCnxns' => $maxClientCnxns,
            'tickTime' => $tickTime,
            'initLimit' => $initLimit,
            'syncLimit' => $syncLimit,
            'dataDir' => $dataDir,
            'zookeepers' => $zookeepers,
            'defaultClientPort' => $defaultClientPort,
            'defaultPeerPort' => $defaultPeerPort,
            'defaultElectionPort' => $defaultElectionPort,
        }),
        require => Package['zookeeper-server'],
    }
    file {"${dataDir}":
        ensure => 'directory',
        owner => 'zookeeper',
        group => 'zookeeper',
        mode => '0755',
        require => Package['zookeeper'],
    }
    service {'zookeeper-server':
        enable => $enable,
        ensure => $service_ensure,
        subscribe => File['/etc/zookeeper/conf/zoo.cfg'],
    }

    file {'/usr/lib/systemd/system/hadoop-hdfs-zkfc.service':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/usr/lib/systemd/system/hadoop-hdfs-zkfc.service',
        require => Package['zookeeper'],
        notify  => [Service['hadoop-hdfs-zkfc'], Exec['zookeeper daemon-reload']],
    }

    file {'/etc/sysconfig/hadoop-hdfs-zkfc':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/etc/sysconfig/hadoop-hdfs-zkfc',
        require => Package['zookeeper'],
        notify  => Service['hadoop-hdfs-zkfc'],
    }

    file {'/usr/lib/systemd/system/zookeeper-server.service':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/usr/lib/systemd/system/zookeeper-server.service',
        require => Package['zookeeper'],
        notify  => [Service['zookeeper-server'], Exec['zookeeper daemon-reload']],
    }

    service {'hadoop-hdfs-zkfc': }
}
