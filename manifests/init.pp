class hadoop (
    String $package_ensure,
    String $package_name,
    Optional[String] $package_provider = undef,
    #Array[Enum['none', 'namenode', 'client', 'datanode', 'zfsdatanode', 'journalnode']] $role,
    Array[String] $datanode_dirs,
    Array[String] $namenodes,
    Array[String] $journalnodes,
    String $datanode_mount_pattern,
    String $fs_default_name,
    String $nameservice_id,
    String $conf_dir,
    String $hadoop_tmp_dir,
    String $dfs_umaskmode,
    Integer $io_bytes_per_checksum,
    Integer $io_file_buffer_size,
    String $hadoop_log_dir,
    String $clienttrace,
    Integer $dfs_block_size,
    Integer $dfs_block_scan_period_hours,
    Integer $dfs_replication,
    Integer $dfs_replication_max,
    Integer $dfs_replication_min,
    Integer $dfs_datanode_du_reserved,
    Integer $dfs_balance_bandwidthPerSec,
    Integer $dfs_datanode_handler_count,
    String $dfs_datanode_address,
    String $dfs_datanode_http_address,
    String $dfs_datanode_https_address,
    String $dfs_datanode_ipc_address,
    String $dfs_hosts_exclude,
    Integer $dfs_namenode_handler_count,
    String $dfs_namenode_logging_level,
    String $fs_checkpoint_dir,
    String $dfs_http_address,
    Integer $fs_checkpoint_period,
    String $dfs_permissions_supergroup,
    Integer $ganglia_reporting_interval,
    String $ganglia_servers,
    Hash[String, Struct[{id => Integer,
                         Optional[clientPort] => Integer,
                         Optional[peerPort] => Integer,
                         Optional[electionPort] => Integer}]] $zookeepers = {},
) {
    # Solaris doesn't provide a fact for partitions
    if (size($datanode_dirs) == 0 and $facts['partitions']) {
#        $filtered_data = $facts['partitions'].filter |$part, $part_data| { has_key($part_data, 'mount') and ($part_data['mount'] =~ "/hadoop" or $part_data['label'] =~ "hadoop") }
#	$hdfs_partitions = values($filtered_data).reduce([]) |$memo, $parthash| {
#	    concat($memo, $parthash['mount'])
#	}

        $hdfs_partitions = $facts['partitions'].reduce([]) |$memo, $part_hash| {
            if (has_key($part_hash[1], 'mount') and ($part_hash[1]['mount'] =~ "${datanode_mount_pattern}")) {
                concat($memo, $part_hash[1]['mount'])
            } else {
                $memo
            }
        }

        #notify {"fact":  message => "${facts['partitions']}",}
        #notify {"filtered_data":  message => "${filtered_data}",}
        #notify {"hdfs_partitions":  message => "${hdfs_partitions}",}
	#$hdfs_partitions = $datanode_dirs
    } else {
	$hdfs_partitions = $datanode_dirs
    }

    $zooDefaultClientPort = lookup('hadoop::zookeeper::defaultClientPort')
    $zookeeper_csv = $zookeepers.reduce([]) |$memo, $server| {
        concat($memo, join([$server[0], $server[1]['clientPort']?{Integer => $server[1]['clientPort'], default => $zooDefaultClientPort}], ':'))
    }
    #notify{'zoo_msg': message => "${zookeeper_csv}", }

    exec { '/usr/bin/chown -R hdfs:hdfs /hadoop' :
	refreshonly => true,
    }

    if $package_provider {
        Package { provider => $package_provider, }
    }

    package {"${package_name}": ensure => $package_ensure, }

    exec {"hadoop daemon-reload":
        command     => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    file { "${conf_dir}/core-site.xml":
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('hadoop/core-site.xml.epp', {
            'nameservice_id' => $nameservice_id,
            'fs_default_name' => $fs_default_name,
            'hadoop_tmp_dir' => $hadoop_tmp_dir,
            'dfs_umaskmode' => $dfs_umaskmode,
            'io_bytes_per_checksum' => $io_bytes_per_checksum,
            'io_file_buffer_size' => $io_file_buffer_size,
            'hadoop_log_dir' => $hadoop_log_dir,
            'zookeeper_csv' => $zookeeper_csv,
        }),
        require => Package["${package_name}"],
    }

    file { "${conf_dir}/hdfs-site.xml":
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('hadoop/hdfs-site.xml.epp', {
            'nameservice_id' => $nameservice_id,
            'namenodes' => $namenodes,
            'journalnodes' => $journalnodes,
            'datanode_dirs' => $hdfs_partitions,
            'dfs_block_size' => $dfs_block_size,
            'dfs_block_scan_period_hours' => $dfs_block_scan_period_hours,
            'dfs_replication' => $dfs_replication,
            'dfs_replication_max' => $dfs_replication_max,
            'dfs_replication_min' => $dfs_replication_min,
            'dfs_datanode_du_reserved' => $dfs_datanode_du_reserved,
            'dfs_balance_bandwidthPerSec' => $dfs_balance_bandwidthPerSec,
            'dfs_datanode_handler_count' => $dfs_datanode_handler_count,
            'dfs_hosts_exclude' => $dfs_hosts_exclude,
            'dfs_namenode_handler_count' => $dfs_namenode_handler_count,
            'dfs_namenode_logging_level' => $dfs_namenode_logging_level,
            'fs_checkpoint_dir' => $fs_checkpoint_dir,
            'dfs_http_address' => $dfs_http_address,
            'fs_checkpoint_period' => $fs_checkpoint_period,
            'dfs_permissions_supergroup' => $dfs_permissions_supergroup,
            'dfs_datanode_address' => $dfs_datanode_address,
            'dfs_datanode_http_address' => $dfs_datanode_http_address,
            'dfs_datanode_https_address' => $dfs_datanode_https_address,
            'dfs_datanode_ipc_address' => $dfs_datanode_ipc_address,
        }),
        require => Package["${package_name}"],
    }

    file { "${conf_dir}/log4j.properties":
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('hadoop/log4j.properties.epp', {
            clienttrace => $clienttrace,
        }),
        require => Package["${package_name}"],
    }

    file { "${conf_dir}/hadoop-metrics2.properties":
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('hadoop/hadoop-metrics2.properties.epp', {
            'ganglia_reporting_interval' => $ganglia_reporting_interval,
            'ganglia_servers' => $ganglia_servers,
        }),
        require => Package["${package_name}"],
    }

}
