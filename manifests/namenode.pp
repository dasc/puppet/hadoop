class hadoop::namenode (
    String $package_ensure,
) {
    include ::hadoop

    package {'hadoop-hdfs-zkfc':
        ensure => $package_ensure,
    }
    package {'hadoop-hdfs-namenode':
        ensure => $package_ensure,
    }

    file { ["$::hadoop::hadoop_tmp_dir", "$::hadoop::hadoop_tmp_dir/dfs", "$::hadoop::hadoop_tmp_dir/dfs/name"]:
        ensure => 'directory',
        owner => 'hdfs',
        group => 'hdfs',
        mode => '0755',
        require => Package['hadoop-hdfs-namenode'],
    }

    file {'/usr/lib/systemd/system/hadoop-hdfs-namenode.service':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/usr/lib/systemd/system/hadoop-hdfs-namenode.service',
        require => Package['hadoop-hdfs-namenode'],
        notify  => [Service['hadoop-hdfs-namenode'], Exec['hadoop daemon-reload']],
    }

    file {'/etc/sysconfig/hadoop-hdfs-namenode':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/etc/sysconfig/hadoop-hdfs-namenode',
        require => Package['hadoop-hdfs-namenode'],
        notify  => Service['hadoop-hdfs-namenode'],
    }

    service {'hadoop-hdfs-namenode': }
}
