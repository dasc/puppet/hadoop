class hadoop::client (
    String $package_name,
    String $package_ensure,
) {
    include ::hadoop

    if $facts['os']['family'] == 'Solaris' {
        package {'client-package':
            name => "$package_name",
            ensure => $package_ensure,
            provider => 'pkgutil',
        }
    } else {
        package {'client-package':
            name => "$package_name",
            ensure => $package_ensure,
        }
        package {'hadoop-hdfs-fuse':
            ensure => $package_ensure,
        }
        package {'nagios-plugins-hdfs-fuse':
            ensure => installed,
        }
        file { '/hdfs':
            ensure => 'directory',
            owner => 'root',
            group => 'root',
            mode => '0755',
        }
    }
}
