class hadoop::datanode (
    String $package_ensure,
) {
    include ::hadoop

#    if (size($datanode_dirs) == 0) {
##        $filtered_data = $facts['partitions'].filter |$part, $part_data| { has_key($part_data, 'mount') and ($part_data['mount'] =~ "/hadoop" or $part_data['label'] =~ "hadoop") }
##	$hdfs_partitions = values($filtered_data).reduce([]) |$memo, $parthash| {
##	    concat($memo, $parthash['mount'])
##	}
#
#        $hdfs_partitions = $facts['partitions'].reduce([]) |$memo, $part_hash| {
#	    if (has_key($part_hash[1], 'mount') and ($part_hash[1]['mount'] =~ "${datanode_mount_pattern}")) {
#		concat($memo, $part_hash[1]['mount'])
#	    } else {
#		$memo
#	    }
#	}
#
#
#        #notify {"fact":  message => "${facts['partitions']}",}
#        #notify {"filtered_data":  message => "${filtered_data}",}
#        #notify {"hdfs_partitions":  message => "${hdfs_partitions}",}
#	#$hdfs_partitions = $datanode_dirs
#    } else {
#	$hdfs_partitions = $datanode_dirs
#    }

#    exec { '/sbin/service hadoop-hdfs-datanode condrestart':
#        subscribe => [
#            File['/etc/hadoop/conf/core-site.xml'],
#            File['/etc/hadoop/conf/hdfs-site.xml'],
#            File['/etc/hadoop/conf/hadoop-metrics2.properties'],
#        ],
#        refreshonly => true,
#    }
    package {'hadoop-hdfs-datanode':
        ensure => $package_ensure,
    }
    file { $::hadoop::hdfs_partitions:
        ensure  => 'directory',
        owner   => 'hdfs',
        group   => 'hdfs',
        mode    => '0700',
        require => Package['hadoop-hdfs-datanode'],
        notify  => Exec['/usr/bin/chown -R hdfs:hdfs /hadoop'],
    }

    file {'/usr/lib/systemd/system/hadoop-hdfs-datanode.service':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/usr/lib/systemd/system/hadoop-hdfs-datanode.service',
        require => Package['hadoop-hdfs-datanode'],
        notify  => [Service['hadoop-hdfs-datanode'], Exec['hadoop daemon-reload']],
    }

    file {'/etc/sysconfig/hadoop-hdfs-datanode':
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet://puppet/modules/hadoop/etc/sysconfig/hadoop-hdfs-datanode',
        require => Package['hadoop-hdfs-datanode'],
        notify  => Service['hadoop-hdfs-datanode'],
    }

    Exec['/usr/bin/chown -R hdfs:hdfs /hadoop'] ->
    service {'hadoop-hdfs-datanode': 
    subscribe => File['/etc/hadoop/conf/hadoop-metrics2.properties'],
    }
}
